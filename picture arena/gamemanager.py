from flask import Flask, request
import requests
import random
from Card import *


class gamemanager:
    def __init__(self):        
        self.idRoom = 0
        self.bet = 0
        
        self.idChallenge = 0
        self.idPlayerOne = 0
        self.idPlayerTwo = 0
        self.idWinner = 0
        self.idLoose = 0

        self.factionPlayerOne =""
        self.factionPlayerTwo =""
   
        self.player1deckToAndroid = []
        self.player2deckToAndroid = []

        self.enoughMoney = True
        self.endmatch = False
        
        self.player1deck = []
        self.player2deck = []

        self.fightersplayer1=[]
        self.fightersplayer2=[]

        self.player1equipmentsCard = []
        self.player2equipmentsCard = []


    def recoverChallenge(self, idChallenge) : 
        # api-endpoint
        self.idChallenge = idChallenge
        URL = "http://54.152.148.26:80/challenge/"+str(self.idChallenge)+"/"
        
        # sending get request and saving the response as response object
        r = requests.get(url = URL)
        # extracting data in json format
        data = r.json()
        
        # recover decks player one
        cardsInTeam = data['userTeam']
        equipementInTeam = data['userItems']
        self.bet = data['bet']
        self.idPlayerOne = data['userId']
        self.factionPlayerOne = data['faction']

        # api-endpoint
        URL = "http://204.236.239.109/cardsFight/"+str(self.idPlayerOne)
        
        # sending get request and saving the response as response object
        r = requests.get(url = URL)
        # extracting data in json format
        data = r.json()
        for cardToConvert in data :
            if cardToConvert["id"] in cardsInTeam :
                self.player1deck.append(Card(cardToConvert["name"],cardToConvert["hp"],cardToConvert["attack"],cardToConvert["speedattack"]))
                self.player1deckToAndroid.append(cardToConvert)
            elif cardToConvert["id"] in equipementInTeam : 
                self.player1equipmentsCard.append(Card(cardToConvert["name"],cardToConvert["hp"],cardToConvert["attack"],cardToConvert["speedattack"]))
        
        self.player1equipmentsCard += [0] * (3 - len(self.player1equipmentsCard)) 
        
        return self

    def recoverPlayer(self, login) : 
        # api-endpoint
        URL = "http://54.152.148.26:80/users/"+login
        
        # sending get request and saving the response as response object
        r = requests.get(url = URL)
        # extracting data in json format
        data = r.json()
        
        # recover decks player two
        cardsInTeam = data['currentTeam']
        equipementInTeam = data['equippedItems']
        self.idPlayerTwo = data['userId']
        self.enoughMoney =  data['money'] >= self.bet

        self.factionPlayerTwo = data['faction']
        # api-endpoint
        URL = "http://204.236.239.109/cardsFight/"+str(self.idPlayerTwo)
        
        # sending get request and saving the response as response object
        r = requests.get(url = URL)
        # extracting data in json format
        data = r.json()
        
        cardsToConvert = data
        
        for cardToConvert in cardsToConvert : 
            print(str(cardToConvert["id"])+" in "+str(cardsInTeam))
            if cardToConvert["id"] in cardsInTeam :
                self.player2deck.append(Card(cardToConvert["name"],cardToConvert["hp"],cardToConvert["attack"],cardToConvert["speedattack"]))
                self.player2deckToAndroid.append(cardToConvert)
            elif cardToConvert["id"] in equipementInTeam : 
                self.player2equipmentsCard.append(Card(cardToConvert["name"],cardToConvert["hp"],cardToConvert["attack"],cardToConvert["speedattack"]))
         
        self.player2equipmentsCard += [0] * (3 - len(self.player2equipmentsCard)) 
        
        return self

    def beginFight(self):
        for i in range(len(self.player1deck)):
            self.fightersplayer1.append(self.player1deck[i])
            if(self.player1equipmentsCard[i] != 0) : 
                self.fightersplayer1[i].setEquipment(self.player1equipmentsCard[i])

        for i in range(len(self.player2deck)):
            self.fightersplayer2.append(self.player2deck[i])
            if(self.player1equipmentsCard[i] != 0) :         
               self.fightersplayer2[i].setEquipment(self.player2equipmentsCard[i])
        return self

    def Fight(self):
        ret = "{ \"game\":["
        deckLoose = []
        deckWinner = []
        for i in range (1,10):
            ret = ret + "{\"round" + str(i) +"\": ["
            if self.endmatch == True:
                ret = ret+ "]}"
                break
            for k in range(1,100) :
                if (self.fightersplayer1[0].isDead == False) and (self.fightersplayer2[0].isDead == False) :
                    if(bool(random.getrandbits(1))) : 
                        self.fightersplayer1[0].Attack(self.fightersplayer2[0])
                        ret = ret +"{ \"idAttacker\":\""+str(self.idPlayerOne)+"\",\"attacker\":\""+self.fightersplayer1[0].getName()+"\", \"damage\":"+str(self.fightersplayer1[0].getAttackpoints())+",\"defencer\":\""+self.fightersplayer2[0].getName()+"\",\"life\":"+str(self.fightersplayer2[0].getHp())+"},"                         
                        self.fightersplayer2[0].Attack(self.fightersplayer1[0])
                        ret = ret +"{ \"idAttacker\":\""+str(self.idPlayerTwo)+"\",\"attacker\":\""+self.fightersplayer2[0].getName()+"\", \"damage\":"+str(self.fightersplayer2[0].getAttackpoints())+",\"defencer\":\""+self.fightersplayer1[0].getName()+"\",\"life\":"+str(self.fightersplayer2[0].getHp())+"},"

                    else :
                        self.fightersplayer2[0].Attack(self.fightersplayer1[0])
                        ret = ret +"{ \"idAttacker\":\""+str(self.idPlayerTwo)+"\", \"attacker\":\""+self.fightersplayer2[0].getName()+"\", \"damage\":"+str(self.fightersplayer2[0].getAttackpoints())+",\"defencer\":\""+self.fightersplayer1[0].getName()+"\",\"life\":"+str(self.fightersplayer2[0].getHp())+"},"
                        self.fightersplayer1[0].Attack(self.fightersplayer2[0])
                        ret = ret +"{ \"idAttacker\":\""+str(self.idPlayerOne)+"\", \"attacker\":\""+self.fightersplayer1[0].getName()+"\", \"damage\":"+str(self.fightersplayer1[0].getAttackpoints())+",\"defencer\":\""+self.fightersplayer2[0].getName()+"\",\"life\":"+str(self.fightersplayer2[0].getHp())+"},"                         
                        

                if self.fightersplayer1[0].isDead == True :
                    del self.fightersplayer1[0]
                    if(ret[-1] != "[") :
                        ret = ret[:-1] + "],"
                    else : 
                        ret += "],"
                    if self.fightersplayer1 == []:
                        self.idWinner = self.idPlayerTwo
                        self.idLoose = self.idPlayerOne
                        deckWinner = self.player2deckToAndroid
                        deckLoose = self.player1deckToAndroid
                        self.endmatch = True
                    break

                if self.fightersplayer2[0].isDead == True :
                    del self.fightersplayer2[0]
                    if(ret[-1] != "[") :
                        ret = ret[:-1] + "],"
                    else : 
                        ret += "],"
                    if self.fightersplayer2 == []:
                        self.idWinner = self.idPlayerOne
                        self.idLoose = self.idPlayerTwo
                        deckWinner = self.player1deckToAndroid
                        deckLoose = self.player2deckToAndroid
                        self.endmatch = True
                    break
            ret = ret[:-1]+ "},"
        ret += "]"
        
        # api-endpoint
        URL = "http://54.152.148.26:80/challenge/"+str(self.idChallenge)+"/validate"
        mydata = {}
        mydata["idWinner"] = self.idWinner
        mydata["idLooser"] = self.idLoose
        ret += ", \"result\":{\"idWinner\":"+str(self.idWinner)+",\"idLoose\":"+str(self.idLoose)+"},  \"deckWinner\":"+str(deckWinner).replace("'","\"")+",\"deckLooser\":"+str(deckLoose).replace("'","\"")+"}"
        # sending get request and saving the response as response object
        r = requests.delete(url = URL, params=mydata)
        url = 'http://localhost:4000/notif'
        if(self.idPlayerOne == self.idWinner) :
            result = "win"
        else : 
            result = "loose"
        correct_payload = {'idPlayer': self.idPlayerOne, 'bet': self.bet, 'ennemie': self.idPlayerTwo, 'result':result}
        # Output => OK 

        r = requests.post(url, data=correct_payload)
        return ret
